from flask import Flask, jsonify

app = Flask(__name__)
app.config['DEBUG'] = True

list = [
    {
        'id': 0,
        'name': 'Arepa',
        'price': 3.5
    },
{
        'id': 1,
        'name': 'Papa Rellena',
        'price': 5
    },
{
        'id': 2,
        'name': 'Soda',
        'price': 1
    },{
        'id': 3,
        'name': 'Chicheme',
        'price': 5
    },
{
        'id': 4,
        'name': 'Pan',
        'price': 13.5
    },

]

@app.route('/', methods=['GET'])
def home():
    return "<h1> Mi API de TALLER DE SERVIDORES >.< </h1>"

@app.route('/api/v1/comidas', methods=['GET'])
def service():
    return jsonify(list)


if __name__ == '__main__':
    app.run()
